/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.nontimed.WarningEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.formatters.TextFormatter;
import me.ctharvey.mdbase.logger.LogHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class CommandWarn extends Command {

    public CommandWarn() {
        super(Bane.getPlugin(), "Warn");
    }

    /**
     * Lets have the onCommand verify the parameters we need. Then we'll pass it
     * off to make sure the correct server handles it.
     *
     * @param cs
     * @param cmnd
     * @param string
     * @param strings
     * @return
     */
    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        try {
            switch (strings.length) {
                case 0:
                    Bane.getPlugin().sendMsg(cs, "You must specify a user to message them.");
                    break;
                case 1:
                    Bane.getPlugin().sendMsg(cs, "Great job you specified someone to warn.  Try again and add a reason.");
                    break;
                default:
                    String message = TextFormatter.join(strings, 1);
                    PlayerID affected = PlayerIDHandler.getPlayerID(strings[0]);
                    WarningEvent event = new WarningEvent(message, PlayerIDHandler.getPlayerID(cs.getName()), affected, LogHandler.getLogsFromFile(affected));
                    BaneController.processEvent(event);
            }
        } catch (NoIdException ex) {
            Bane.getPlugin().sendMsg(cs, ex.getMessage());
        }
        return true;
    }

    
}
