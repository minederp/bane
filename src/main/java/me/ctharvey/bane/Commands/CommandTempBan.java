/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.timed.TempBanEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.formatters.TextFormatter;
import me.ctharvey.mdbase.logger.LogHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class CommandTempBan extends Command {

    public CommandTempBan() {
        super(Bane.getPlugin(), "tempban");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        switch (strings.length) {
            case 0:
                Bane.getPlugin().sendMsg(cs, "The correct way to use the command is as follows:");
                Bane.getPlugin().sendMsg(cs, "'/tempban <playername> <#> <type: s/m/d> <reason>");
                break;
            case 1:
                Bane.getPlugin().sendMsg(cs, "Specify a time frame and reason.");
                break;
            case 2:
                Bane.getPlugin().sendMsg(cs, "Specify the reason for the tempban.");
                break;
            case 3:
                Bane.getPlugin().sendMsg(cs, "Please use more than one word for a reason.  You have not provided enough info.");
                break;
            default:
                String playerName = strings[0];
                if (!StringUtils.isNumeric(strings[1])) {
                    Bane.getPlugin().sendMsg(cs, "You provided a non number for time.  format is:");
                    Bane.getPlugin().sendMsg(cs, "'/tempban <playername> <#> <type: s/m/d> <reason>");
                    return true;
                }
                int timeValue = Integer.valueOf(strings[1]);
                char typeOfTime = strings[2].toLowerCase().charAt(0);
                String message = TextFormatter.join(strings, " ", 3);
                int multiplier = 1;
                switch (typeOfTime) {
                    case 's':
                        break;
                    case 'm':
                        multiplier = 60;
                        break;
                    case 'h':
                        multiplier = 60 * 60;
                        break;
                    case 'd':
                        multiplier = 60 * 60 * 24;
                        break;
                    default:
                        Bane.getPlugin().sendMsg(cs, "System doesn't recognize the interval of time used.  Please use s m h d.");
                        break;

                }
                long duration = multiplier * timeValue;
                try {
                    PlayerID affected = PlayerIDHandler.getPlayerID(playerName);
                    TempBanEvent be = new TempBanEvent(message, PlayerIDHandler.getPlayerID(cs.getName()), affected, LogHandler.getLogsFromFile(affected));
                    be.setDuration(duration);
                    BaneController.processEvent(be);
                } catch (NoIdException ex) {
                    Bane.getPlugin().sendMsg(cs, ex.getMessage());
                }
                break;
        }
        return true;
    }

}
