/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BanEvent;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.PermBanEvent;
import me.ctharvey.bane.events.timed.TempBanEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Charles
 */
public class CommandUnban extends Command {

    public CommandUnban() {
        super(Bane.getPlugin(), "unban");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        switch (strings.length) {
            case 0:
                Bane.getPlugin().sendMsg(cs, "The correct usage of this commands is '/unban <playername> reason ...");
                break;
            default:
                Map<BaneEventType, List<BaneEventInterface>> allEvents;
                try {
                    allEvents = Bane.getController().getBaneEvents(PlayerIDHandler.getPlayerID(strings[0]), Arrays.asList(BaneEventType.BAN, BaneEventType.PERMBAN, BaneEventType.TEMPBAN));
                } catch (NoIdException ex) {
                    Bane.getPlugin().sendMsg(cs, "You have chosen someone who does not exist in our system.");
                    return true;
                }
                if (!allEvents.isEmpty()) {
                    int cntUnbanned = 0;
                    for (List<BaneEventInterface> events : allEvents.values()) {
                        for (BaneEventInterface event : events) {
                            if (event.getBaseEvent().isActive()) {
                                switch (event.getBaseEvent().getEventType()) {
                                    case BAN:
                                        cntUnbanned++;
                                        BanEvent bEvent = (BanEvent) event;
                                        bEvent.setActive(false);
                                        bEvent.update();
                                        break;
                                    case TEMPBAN:
                                        cntUnbanned++;
                                        TempBanEvent tEvent = (TempBanEvent) event;
                                        tEvent.setActive(false);
                                        tEvent.setDuration(0);
                                        tEvent.update();
                                        break;
                                    case PERMBAN:
                                        if (cs.isOp()) {
                                            cntUnbanned++;
                                            PermBanEvent pEvent = (PermBanEvent) event;
                                            pEvent.setActive(false);
                                            pEvent.update();
                                            break;
                                        } else {
                                            Bane.getPlugin().sendMsg(cs, "You have attempted to remove a permban.  Only an OP can do this.");
                                        }
                                }
                            }
                        }
                    }
                    Bane.getPlugin().broadcast("Bane.unban", cs.getName() + " has unbanned " + strings[0] + " including " + cntUnbanned + " records.");
                    Bane.getPlugin().log(cs.getName() + " has unbanned " + strings[0] + " including " + cntUnbanned + " records.");
                } else {
                    Bane.getPlugin().broadcast("Bane.unban", "User either not found or is not banned.");
                }
        }
        return true;
    }
}
