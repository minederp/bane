/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import java.util.List;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.FlagEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Charles
 */
public class CommandUnFlag extends Command {

    public CommandUnFlag() {
        super(Bane.getPlugin(), "unflag");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        try {
            switch (strings.length) {
                case 0:
                    Bane.getPlugin().sendMsg(cs, "Correct usage is \"/unflag playername");
                    break;
                default:
                    List<FlagEvent> eventType = Bane.getController().getEventType(PlayerIDHandler.getPlayerID(strings[0]), BaneEventType.FLAG, FlagEvent.class);
                    for (BaneEventInterface be : eventType) {
                        FlagEvent fe = (FlagEvent) be;
                        if (fe.isActive()) {
                            Bane.getPlugin().log("&2Flag removed: Player -- &6" + fe.getAffected().getName() + " &2for &6" + fe.getReason() + " &2 by &c" + cs.getName());
                            fe.setActive(false);
                            fe.update();
                        }
                    }
                    break;
            }
        } catch (NoIdException ex) {
            Bane.getPlugin().sendMsg(cs, ex.getMessage());

        }
        return true;
    }
}
