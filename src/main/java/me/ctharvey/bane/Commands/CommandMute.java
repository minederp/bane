/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.bane.events.timed.MuteEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.formatters.TextFormatter;
import me.ctharvey.mdbase.logger.LogHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class CommandMute extends Command {

    public CommandMute() {
        super(Bane.getPlugin(), "mute");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        try {
            switch (strings.length) {
                case 0:
                    Bane.getPlugin().sendMsg(cs, "Correct usage is \"/mute playername 1 d\'");
                    break;
                case 1:
                    Bane.getPlugin().sendMsg(cs, "Please specify a length for muting.");
                    break;
                case 2:
                    Bane.getPlugin().sendMsg(cs, "Please specify a type of unit 'm d w'");
                    break;
                //Now you have /mute playername 1 d
                case 3:
                    Bane.getPlugin().sendMsg(cs, "Please specify reason for mute.");
                    break;
                default:
                    if (!StringUtils.isNumeric(strings[1])) {
                        Bane.getPlugin().sendMsg(cs, "You provided a non number for time.  format is:");
                        Bane.getPlugin().sendMsg(cs, "'/tempban <playername> <#> <type: s/m/d> <reason>");
                        return true;
                    }
                    String playername = strings[0];
                    int amount = Integer.valueOf(strings[1]);
                    char type = strings[2].charAt(0);
                    switch (type) {
                        case 'm':
                            amount = amount * 60;
                            break;
                        case 'h':
                            amount = amount * 60 * 60;
                            break;
                        case 'd':
                            amount = amount * 60 * 60 * 24;
                            break;
                        case 'w':
                            amount = amount * 60 * 60 * 24 * 7;
                            break;
                    }

                    PlayerID affected = PlayerIDHandler.getPlayerID(playername);
                    String msg = TextFormatter.join(strings, " ", 3);
                    MuteEvent event = new MuteEvent(msg, PlayerIDHandler.getPlayerID(cs.getName()), affected, LogHandler.getLogsFromFile(affected));
                    event.setDuration(amount);
                    BaneController.processEvent(event);
                    break;
            }
        } catch (NoIdException ex) {
            Bane.getPlugin().sendMsg(cs, ex.getMessage());

        }
        return true;
    }
}
