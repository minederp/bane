/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.KickEvent;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.formatters.TextFormatter;
import me.ctharvey.mdbase.logger.LogHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class CommandKick extends Command {

    public CommandKick() {
        super(Bane.getPlugin(), "kick");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        switch (strings.length) {
            case 0:
                Bane.getPlugin().sendMsg(cs, "Correct usage is \"/kick playername reason\'");
                break;
            case 1:
                Bane.getPlugin().sendMsg(cs, "Please specify a reason for kicking.");
                break;
            case 2:
                Bane.getPlugin().sendMsg(cs, "You need more than one word as a reason for kicking a player.");
                break;
            default:
                String message = TextFormatter.join(strings, 1);
                try {
                    PlayerID affected = PlayerIDHandler.getPlayerID(strings[0]);
                    KickEvent event = new KickEvent(message, PlayerIDHandler.getPlayerID(cs.getName()), affected, LogHandler.getLogsFromFile(affected));
                    BaneController.processEvent(event);
                } catch (NoIdException ex) {
                    Bane.getPlugin().sendMsg(cs, ex.getMessage());
                }
                break;
        }
        return true;
    }

    
}
