/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.bane.events.timed.TimedEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class CommandBanLog extends Command {

    public CommandBanLog() {
        super(Bane.getPlugin(), "banlog");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {

        List<BaneEventType> types = determineTypesToGet(cs, strings);
        if (!types.isEmpty()) {
            try {
                Map<BaneEventType, List<BaneEventInterface>> baneInfo = Bane.getController().getBaneEvents(PlayerIDHandler.getPlayerID(strings[0]));
                Bane.getPlugin().sendMsg(cs, "-----Bane Log for " + strings[0] + "-----");
//                if (isbanned(baneInfo.get(BaneEventType.BAN))) {
//                    Bane.getPlugin().sendMsg(cs, "Is currently banned.");
//                } else {
//                    Bane.getPlugin().sendMsg(cs, "Is not currently banned.");
//                }
                for (BaneEventType bt : BaneEventType.values()) {
                    if (types.contains(bt)) {
                        int size = 0;
                        if (baneInfo.get(bt) != null) {
                            size = baneInfo.get(bt).size();
                        }
                        Bane.getPlugin().sendMsg(cs, bt.getColor() + "" + bt + ": " + size);
                        if (baneInfo.get(bt) != null) {
                            for (BaneEventInterface event : baneInfo.get(bt)) {
                                NonTimedEvent be = event.getBaseEvent();
                                if (be != null) {
                                    long time = be.getTime() * 1000L;
                                    Date date = new Date(time);
                                    SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yy hh:mm:ss");
                                    switch (be.getEventType()) {
                                        case BAN:
                                        case FLAG:
                                        case PERMBAN:
                                            Bane.getPlugin().sendMsg(cs, dt.format(date) + " #" + bt.getColor() + be.getId() + " " + be.getEventType() + ChatColor.WHITE + " by " + be.getInitiatedBy().getName() + " for " + be.getReason() + " and active = " + be.isActive());
                                            break;
                                        case MUTE:
                                        case TEMPBAN:
                                            Bane.getPlugin().sendMsg(cs, dt.format(date) + " #" + bt.getColor() + be.getId() + " " + be.getEventType() + ChatColor.WHITE + " by " + be.getInitiatedBy().getName() + " for " + be.getReason() + " and active = " + be.isActive());
                                            break;
                                        default:
                                            Bane.getPlugin().sendMsg(cs, dt.format(date) + " #" + bt.getColor() + be.getId() + " " + be.getEventType() + ChatColor.WHITE + " by " + be.getInitiatedBy().getName() + " for " + be.getReason());
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (NoIdException ex) {
                Bane.getPlugin().sendMsg(cs, ex.getMessage());
            }

        }
        return true;
    }
    //
//    @Override
//    public void run(NonTimedEvent event) {
//        super.run(event);
//    }
//
//    @Override
//    public void commandFail(NonTimedEvent event, String reason) {
//        super.commandFail(event, reason);
//    }
//
//    private HashMap<BaneEventType, Integer> getNumInfractions(HashMap<BaneEventType, List<NonTimedEvent>> baneInfo) {
//        HashMap<BaneEventType, Integer> numInfractions = new HashMap<>();
//        for (Map.Entry<BaneEventType, List<NonTimedEvent>> entry : baneInfo.entrySet()) {
//            if (numInfractions.containsKey(entry.getKey())) {
//                numInfractions.put(entry.getKey(), numInfractions.get(entry.getKey()) + 1);
//            } else {
//                numInfractions.put(entry.getKey(), 1);
//            }
//        }
//        return numInfractions;
//    }
//
//    private boolean isbanned(List<NonTimedEvent> baneInfo) {
//        if (baneInfo != null && !baneInfo.isEmpty()) {
//            for (NonTimedEvent BE : baneInfo) {
//                if (BE instanceof BanEvent) {
//                    BanEvent banEvent = (BanEvent) BE;
//                    if (banEvent.isActive()) {
//                        return true;
//                    }
//                }
//            }
//        }
//        return false;
//    }
//
//    private HashMap<BaneEventType, List<NonTimedEvent>> sortResults(List<NonTimedEvent> baneInfo) {
//        HashMap<BaneEventType, List<NonTimedEvent>> sortedResults = new HashMap();
//        for (NonTimedEvent be : baneInfo) {
//            if (!sortedResults.containsKey(be.getEventType())) {
//                sortedResults.put(be.getEventType(), new ArrayList());
//            }
//            sortedResults.get(be.getEventType()).add(be);
//        }
//        return sortedResults;
//    }

    private List<BaneEventType> determineTypesToGet(CommandSender cs, String[] strings) {
        List<BaneEventType> types = new ArrayList();
        switch (strings.length) {
            case 0:
                Bane.getPlugin().sendMsg(cs, "Use banlog like: '/banlog player type(optional)'");
                break;
            case 1:
                types = new ArrayList(Arrays.asList(BaneEventType.values()));
                if (!cs.hasPermission("bane.flag")) {
                    types.remove(BaneEventType.FLAG);
                }
                break;
            case 2:
                switch (strings[1].toLowerCase().charAt(0)) {
                    case 'b':
                    case 't':
                    case 'p':
                        types.add(BaneEventType.BAN);
                        types.add(BaneEventType.TEMPBAN);
                        types.add(BaneEventType.PERMBAN);
                        break;
                    case 'w':
                        types.add(BaneEventType.WARN);
                        break;
                    case 'k':
                        types.add(BaneEventType.KICK);
                        break;
                    case 'f':
                        if (((Player) cs).hasPermission("bane.flag")) {
                            types.add(BaneEventType.FLAG);
                        } else {
                            types.clear();
                            Bane.getPlugin().sendMsg(cs, "You do not have permission to see flags.");
                            break;
                        }
                        break;
                    case 'm':
                        types.add(BaneEventType.MUTE);
                        break;
                    default:
                        break;
                }
                break;
        }
        return types;
    }
}
