/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import java.util.List;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.controllers.ChatController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.timed.MuteEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Charles
 */
public class CommandUnmute extends Command {

    public CommandUnmute() {
        super(Bane.getPlugin(), "unmute");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        try {
            switch (strings.length) {
                case 0:
                    Bane.getPlugin().sendMsg(cs, "Correct usage is \"/unmute playername");
                    break;
                default:
                    List<MuteEvent> eventType = Bane.getController().getEventType(PlayerIDHandler.getPlayerID(strings[0]), BaneEventType.MUTE, MuteEvent.class);
                    for (BaneEventInterface be : eventType) {
                        MuteEvent mE = (MuteEvent) be;
                        if (mE.isActive()) {

                            unMute(mE);
                        }
                    }
                    break;
            }
            return true;
        } catch (NoIdException ex) {
            Bane.getPlugin().sendMsg(cs, ex.getMessage());

        }
        return true;
    }

    public void unMute(MuteEvent event) {
        event.setActive(false);
        event.update();
        ChatController.getCurrentlyMuted().remove(event.getBaseEvent().getAffected().getId());
        Bane.getPlugin().sendMsg(Bukkit.getPlayer(event.getBaseEvent().getAffected().getUUID()), "You have been unmuted.");
        Bane.getPlugin().broadcast("bane.view", event.getBaseEvent().getAffected().getName() + " unmuted."
        );
    }

}
