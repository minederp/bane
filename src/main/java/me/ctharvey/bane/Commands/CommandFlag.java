/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.nontimed.FlagEvent;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.formatters.TextFormatter;
import me.ctharvey.mdbase.logger.LogHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler.NoIdException;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
public class CommandFlag extends Command {

    public CommandFlag() {
        super(Bane.getPlugin(), "flag");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        try {
            switch (strings.length) {
                case 0:
                    checkCurrentFlags(cs);
                    break;
                case 1:
                    Bane.getPlugin().sendMsg(cs, "You need to include a reason.");
                    break;
                default:
                    PlayerID affected = PlayerIDHandler.getPlayerID(strings[0]);
                    BaneController.processEvent(new FlagEvent(TextFormatter.join(strings, " ", 1), PlayerIDHandler.getPlayerID(cs.getName()), affected, LogHandler.getLogsFromFile(affected)));
            }
        } catch (NoIdException ex) {
            Bane.getPlugin().sendMsg(cs, ex.getMessage());
        }
        return true;
    }

    public void checkCurrentFlags(CommandSender cs) {
        Collection<FlagEvent> onlineFlaggedUsers = Bane.getController().getFlagController().getCurrentFlags().values();
        if (onlineFlaggedUsers != null && !onlineFlaggedUsers.isEmpty()) {
            for (FlagEvent fe : onlineFlaggedUsers) {
                Bane.getPlugin().sendMsg(cs, "&2Flag: &6" + fe.getBaseEvent().getAffected().getName() + " &2by &c" + fe.getBaseEvent().getAffected().getName() + " &2for &6" + fe.getBaseEvent().getReason());
            }
        } else {
            Bane.getPlugin().sendMsg(cs, "Nobody online is currently flagged.");
        }
    }
}
