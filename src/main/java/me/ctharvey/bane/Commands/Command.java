/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.Bane.Commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.Bane.Bane;
import me.ctharvey.Bane.Events.BE;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Charles
 */
abstract public class Command implements CommandExecutor {

    private String name;

    private HashMap<String, PlayerID> cache = new HashMap();

    public Command(String name) {
        this.name = name;
        Bane.registerCommand(this);
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        return true;
    }

    public String getName() {
        return name;
    }

    public void run(BE event) {
    }

    ;
    public void commandFail(BE event, String reason) {
    }

    public PlayerID getPlayerID(String name) throws NoIdException {
        Bane.Log(new Date().toString());
        if (cache.containsKey(name)) {
            Bane.Log(new Date().toString());
            return cache.get(name);
        }
        try {
            Player player = Bukkit.getPlayer(name);
            PlayerID id = null;
            if (player != null) {
                id = PlayerIDS.get(player);
            } else {
                List<PlayerID> get;
                get = PlayerIDS.get(name);
                if (get != null) {
                    id = get.get(0);
                }
            }
            if (id == null) {
                Player player1 = Bukkit.getOfflinePlayer(name).getPlayer();
                if (player1 != null) {
                    id = PlayerIDS.get(player1);
                }
            }
            if (id == null) {
                throw new NoIdException();
            }
            cache.put(name, id);
            Bane.Log(new Date().toString());
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(Command.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public class NoIdException extends Exception {

        public NoIdException() {
            super("Unable to locate player.  Make sure they have logged into our system at least once.");
        }

    }
}
