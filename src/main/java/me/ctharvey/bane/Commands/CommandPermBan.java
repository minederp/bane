/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.commands;

import com.adamantdreamer.foundation.core.db.PlayerID;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BanEvent;
import me.ctharvey.bane.events.nontimed.PermBanEvent;
import me.ctharvey.mdbase.command.Command;
import me.ctharvey.mdbase.formatters.TextFormatter;
import me.ctharvey.mdbase.logger.LogHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Charles
 */
public class CommandPermBan extends Command {

    public CommandPermBan() {
        super(Bane.getPlugin(), "permban");
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command cmnd, String string, String[] strings) {
        switch (strings.length) {
            case 0:
                Bane.getPlugin().sendMsg(cs, "The correct usage of this commands is '/permban <playername> reason ...");
                break;
            case 1:
                Bane.getPlugin().sendMsg(cs, "Please use more than one word for the reason.");
                break;
            default:
                String message = TextFormatter.join(strings, 1);
                try {
                    PlayerID affected = PlayerIDHandler.getPlayerID(strings[0]);
                    PermBanEvent baneEvent = BaneEventType.PERMBAN.getBaneEvent(PermBanEvent.class, message, PlayerIDHandler.getPlayerID(cs.getName()), affected, LogHandler.getLogsFromFile(affected));
                    BaneController.processEvent(baneEvent);
                    break;
                } catch (PlayerIDHandler.NoIdException ex) {
                    Bane.getPlugin().sendMsg("Unable to locate a player ID for this user.", cs);
                }
        }
        return true;
    }
}
