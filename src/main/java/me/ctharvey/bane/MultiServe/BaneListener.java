/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.multiserve;

import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.mdbase.multiserver.BroadcastPacket;
import me.ctharvey.multiserv.client.PacketHandler;
import me.ctharvey.multiserv.packet.Packet;

/**
 *
 * @author Charles
 */
public class BaneListener extends PacketHandler {

    public BaneListener() {
        super("Bane");
    }

    @Override
    public void run(Packet packet) {
        if (packet instanceof BanePacket) {
            BanePacket bPacket = (BanePacket) packet;
            NonTimedEvent baseEvent = bPacket.getData().getBe().getBaseEvent();
            BaneEventInterface bei = baseEvent.getEventType().getBaneEvent(baseEvent);
            BaneController.processEvent(bei, false);
        }
    }

}
