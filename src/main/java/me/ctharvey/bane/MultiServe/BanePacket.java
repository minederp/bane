/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.multiserve;

import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.FlagEvent;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.mdbase.multiserver.MultiServerHandler;
import me.ctharvey.multiserv.client.MultiServeClient;
import me.ctharvey.multiserv.client.PacketHandler;
import me.ctharvey.multiserv.packet.JSONable;
import me.ctharvey.multiserv.packet.Packet;
import org.json.simple.JSONObject;

/**
 *
 * @author Charles
 */
public class BanePacket extends Packet<BanePacket.BEContainer> {

    public BanePacket() {
    }

    public BanePacket(String serverTo, String serverFrom, BEContainer data) {
        super(BanePacket.class, serverTo, serverFrom, PacketType.BANE, data);
    }

    @Override
    public BEContainer getData() {
        BEContainer event = (BEContainer) super.getStoredData();
        return event;
    }

    @Override
    public void processData(JSONObject jo2) {
        BEContainer beContainer = new BEContainer();
        beContainer.fromJSONObject(jo2);
        this.setData(beContainer);
    }

    public static class BEContainer implements JSONable {

        private BaneEventType type;
        private BaneEventInterface be;

        public BEContainer() {
        }

        public BEContainer(BaneEventType type, NonTimedEvent be) {
            this.type = type;
            this.be = be;
        }

        @Override
        public JSONObject toJSONObject() {
            JSONObject json = new JSONObject();
            json.put("type", type.name());
            json.put("be", be.toJSONObject());
            return json;
        }

        @Override
        public void fromJSONObject(JSONObject jo) {
            type = BaneEventType.getByType((String) jo.get("type"));
            be = new NonTimedEvent();
            be.fromJSONObject((JSONObject) jo.get("be"));
        }

        @Override
        public void fromString(String string) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        public BaneEventInterface getBe() {
            return be;
        }

        public BaneEventType getType() {
            return type;
        }

    }

}
