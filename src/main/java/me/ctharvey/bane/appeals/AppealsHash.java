/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.bane.appeals;

import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.define.Var;
import me.ctharvey.quormdata.structures.QuormObject;

/**
 *
 * @author thronecth
 */
public class AppealsHash extends QuormObject.QuormUniqueIDObject{
    
    @Var @Size(5)
    private String hash;
    private int baneID;

    public AppealsHash() {
    }

    public AppealsHash(String hash, int baneID) {
        this.hash = hash;
        this.baneID = baneID;
    }
    
    @Override
    public Class getClassType() {
        return this.getClass();
    }

    @Override
    public boolean update() {
        return false;
    }

    @Override
    public boolean delete() {
        return false;
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
