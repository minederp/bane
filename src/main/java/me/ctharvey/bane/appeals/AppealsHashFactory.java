/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.bane.appeals;

import java.util.Map;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.quormdata.factories.QuormObjectFactory;

/**
 *
 * @author thronecth
 */
public class AppealsHashFactory extends QuormObjectFactory<AppealsHash> {

    public AppealsHashFactory(QuormHandler handler) {
        super(handler, AppealsHash.class);
    }

    @Override
    public Map<Integer, ? extends AppealsHash> getIndex() {
        return index;
    }
    
}
