/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.commands.CommandBan;
import me.ctharvey.bane.commands.CommandBanLog;
import me.ctharvey.bane.commands.CommandFlag;
import me.ctharvey.bane.commands.CommandKick;
import me.ctharvey.bane.commands.CommandMute;
import me.ctharvey.bane.commands.CommandPermBan;
import me.ctharvey.bane.commands.CommandTempBan;
import me.ctharvey.bane.commands.CommandUnFlag;
import me.ctharvey.bane.commands.CommandUnban;
import me.ctharvey.bane.commands.CommandUnmute;
import me.ctharvey.bane.commands.CommandWarn;
import me.ctharvey.bane.multiserve.BaneListener;
import me.ctharvey.bane.multiserve.BanePacket;
import me.ctharvey.mdbase.MDDatabasePlugin;
import me.ctharvey.mdbase.chat.LocalSend;
import me.ctharvey.mdbase.multiserver.MultiServerHandler;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author Charles
 */
public class Bane extends MDDatabasePlugin {

    private static Bane pl;
    private static final BaneListener listener = new BaneListener();
    private static BaneController controller;
    private static final Set<Class> commands = new HashSet();

    public Bane() {
        super("Bane", ChatColor.RED, "[", "]", new LocalSend());
        commands.add(CommandPermBan.class);
        commands.add(CommandBan.class);
        commands.add(CommandBanLog.class);
        commands.add(CommandFlag.class);
        commands.add(CommandKick.class);
        commands.add(CommandMute.class);
        commands.add(CommandTempBan.class);
        commands.add(CommandUnFlag.class);
        commands.add(CommandUnban.class);
        commands.add(CommandUnmute.class);
        commands.add(CommandWarn.class);
    }

    public static Bane getPlugin() {
        return pl;
    }

    public static BaneListener getListener() {
        return listener;
    }

    @Override
    public void startup() {
        Bane.pl = this;
        MultiServerHandler.registerListener(BanePacket.class, listener);
        controller = new BaneController(dbHandler);
        getServer().getPluginManager().registerEvents(controller, pl);
    }

    @Override
    public void shutdown() {
    }

    public static BaneController getController() {
        return controller;
    }
    
    public void sendMsg(CommandSender cs, String msg){
        sendMsg(msg, cs);
    }

    @Override
    public List<CommandInterface> getCommands() {
        List<CommandInterface> newCommands = new ArrayList();
        for(Class<? extends CommandInterface> commandClass: commands){
            try {
                newCommands.add(commandClass.newInstance());
            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(Bane.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return newCommands;
    }
    
    

}
