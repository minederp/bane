package me.ctharvey.bane.events.timed;
// Generated Jan 18, 2013 1:07:10 AM by Hibernate Tools 3.2.1.GA

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.controllers.ChatController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerLoginEvent;

public final class MuteEvent extends TimedEvent {

    public MuteEvent(String reason, PlayerID initiatedBy, PlayerID affected, JSONableList logs) {
        super(reason, initiatedBy, affected, BaneEventType.MUTE, logs);
    }

    public MuteEvent() {
    }

    @Override
    public void run() {
        ChatController.getCurrentlyMuted().put(getBaseEvent().getAffected().getId(), this);
        if (Bukkit.getPlayer(getBaseEvent().getAffected().getUUID()) != null) {
            String timeAmount = getTimeLeft();
            Bane.getPlugin().sendMsg(Bukkit.getPlayer(getBaseEvent().getAffected().getUUID()), "You were muted by " + getBaseEvent().getInitiatedBy().getName() + " for " + timeAmount);
            Bane.getPlugin().broadcast("bane.mute", "&2Mute: &6" + getBaseEvent().getAffected().getName() + " &2by &c" + getBaseEvent().getInitiatedBy().getName() + " &2for &6" + getBaseEvent().getReason());
        }
    }

    @Override
    public void fail() {
        String reason = "Unable to locate player.";
        if (getBaseEvent().getInitiatedBy().getName().equals("CONSOLE")) {
            Bukkit.getServer().getConsoleSender().sendMessage(reason);
            return;
        }
        Bane.getPlugin().sendMsg(Bukkit.getPlayer(getBaseEvent().getInitiatedBy().getUUID()), reason);
        run();
    }

    @Override
    public void onLogin(Player player, PlayerLoginEvent e) {
        if (this.isActive()) {
            if (this.getUntil() > BaneController.getCurrentUnixStamp()) {
                try {
                    ChatController.getCurrentlyMuted().put(PlayerIDS.get(player).getId(), this);
                    Bane.getPlugin().broadcast("banish.flag", "[&cLogin&f] &2Mute: &6" + getBaseEvent().getAffected().getName() + " &2by &c" + getBaseEvent().getInitiatedBy().getName() + " &2for &6" + getBaseEvent().getReason());
                } catch (SQLException ex) {
                    Logger.getLogger(MuteEvent.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                this.setActive(false);
                this.update();
            }
        }
    }

}
