/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.events.timed;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.quorm.define.Ignore;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import me.ctharvey.mdbase.formatters.TimeFormatter;
import me.ctharvey.quormdata.factories.QuormUniqueNameObjectFactory;
import me.ctharvey.quormdata.structures.QuormObject;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.json.simple.JSONObject;

/**
 *
 * @author thronecth
 */
public class TimedEvent extends QuormObject.QuormUniqueIDObject implements BaneEventInterface {

    private long until;
    private int baneEventID;
    @Ignore
    private transient NonTimedEvent baneEvent;

    public TimedEvent() {
    }

    public TimedEvent(String reason, PlayerID initiatedBy, PlayerID affected, BaneEventType eventType, JSONableList logs) {
        baneEvent = new NonTimedEvent(reason, initiatedBy, affected, eventType, logs);
        baneEvent.setActive(true);
    }

    public boolean isActive() {
        return this.getBaseEvent().isActive();
    }

    public void setUntil(long until) {
        this.until = until;
    }

    public void setBaneEventID(int baneEventID) {
        this.baneEventID = baneEventID;
    }

    public long getUntil() {
        return this.until;
    }

    public void setDuration(long duration) {
        this.until = BaneController.getCurrentUnixStamp() + duration;
        if (duration == 0) {
            this.until = 0;
        }
    }

    public String getTimeLeft() {
        long timeLeft =  (this.until - BaneController.getCurrentUnixStamp());
        return TimeFormatter.formatTime(timeLeft);
    }

    public BaneEventType getEventType() {
        return getBaseEvent().getEventType();
    }

    public final void setActive(boolean b) {
        this.getBaseEvent().setActive(b);
    }

    @Override
    public Class getClassType() {
        return this.getClass();
    }

    @Override
    public boolean update() {
        return this.getBaseEvent().update();
    }

    @Override
    public boolean delete() {
        try {
            return Bane.getController().getTimedFactory().delete(this);
        } catch (SQLException | QuormUniqueNameObjectFactory.InvalidQuormObjectException ex) {
            return false;
        }
    }

    @Override
    public Object clone() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public NonTimedEvent getBaseEvent() {
        if (this.baneEvent == null) {
            baneEvent = Bane.getController().getNonTimedFactory().get(NonTimedEvent.class, baneEventID);
        }
        return baneEvent;

    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject jo = new JSONObject();
        jo.put("baneEventID", getBaseEvent().getId());
        return jo;
    }

    @Override
    public void fromJSONObject(JSONObject jo) {
        throw new UnsupportedOperationException("Instantiation should take place through nontimed event."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean create() {
        try {
            this.getBaseEvent().create();
            this.setBaneEventID(this.getBaseEvent().getId());
            Bane.getController().getTimedFactory().add(TimedEvent.class, this);
            return true;
        } catch (SQLException | QuormUniqueNameObjectFactory.InvalidQuormObjectException ex) {
            return false;
        }
    }

    @Override
    public void run() {
    }

    @Override
    public void fail() {
    }

    @Override
    public void onLogin(Player player, PlayerLoginEvent event) {
    }

    @Override
    public void fromString(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
