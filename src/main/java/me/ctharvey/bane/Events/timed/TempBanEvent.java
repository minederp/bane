/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.events.timed;

import com.adamantdreamer.foundation.core.db.PlayerID;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BanEvent;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 *
 * @author thronecth
 */
public class TempBanEvent extends TimedEvent {
    
    public TempBanEvent(String reason, PlayerID initiatedBy, PlayerID affected, JSONableList logs) {
        super(reason, initiatedBy, affected, BaneEventType.TEMPBAN, logs);
    }
    
    public TempBanEvent() {
    }
    
    @Override
    public void run() {
        NonTimedEvent event = this.getBaseEvent();
        if (Bukkit.getPlayer(event.getAffected().getUUID()) != null) {
            Bane.getController().asyncKickPlayer(getBaseEvent().getInitiatedBy(), getBaseEvent().getAffected(), ChatColor.RED + "You were tempbanned by " + event.getInitiatedBy().getName() + " for " + getTimeLeft() + " -- " + event.getReason());
        }
        Bane.getPlugin().broadcast("banish.view", "&2Tempban: &6" + event.getAffected().getName() + "&2 by &c" + event.getInitiatedBy().getName() + " &2for &c" + event.getReason() + " &2-- &c" + getTimeLeft());
    }
    
    @Override
    public void fail() {
        NonTimedEvent event = this.getBaseEvent();
        if (event.getInitiatedBy().getName().toLowerCase().equals("console")) {
            Bane.getPlugin().log("Unable to locate player currently online.  Banning offline.");
        } else {
            Bane.getPlugin().sendMsg(Bukkit.getPlayer(event.getInitiatedBy().getUUID()), "Unable to locate player currently online.  Banning offline.");
        }
        run();
    }
    
    @Override
    public void onLogin(Player player, PlayerLoginEvent e) {
        if (BaneController.getCurrentUnixStamp() > getUntil()) {
            this.setActive(false);
            this.update();
        } else {
            executeSoftBan(e);
        }
    }
    
    private void executeSoftBan(PlayerLoginEvent e) {
        String message = ChatColor.RED + "Temporarily banned by " + this.getBaseEvent().getInitiatedBy().getName() + " for " + this.getBaseEvent().getReason() + " for " + getTimeLeft();
        e.setKickMessage(message);
        e.disallow(PlayerLoginEvent.Result.KICK_BANNED, message);
//        e.getPlayer().kickPlayer(ChatColor.RED + message);
        Bane.getPlugin().log(ChatColor.RED + "Disconnecting " + this.getBaseEvent().getAffected().getName() + ": Temporarily banned by " + this.getBaseEvent().getInitiatedBy().getName() + " for " + this.getBaseEvent().getReason() + " for " + getTimeLeft());
    }
}
