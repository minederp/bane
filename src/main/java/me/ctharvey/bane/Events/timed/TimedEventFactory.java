/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.events.timed;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.quorm.query.PreparedQuery;
import com.adamantdreamer.quorm.query.Query;
import com.adamantdreamer.quorm.query.QueryResult;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.bane.events.nontimed.NonTimedEventFactory;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.mdbase.database.QuormQueryBuilder;
import me.ctharvey.mdbase.enums.list.ListHandler;
import me.ctharvey.quormdata.factories.QuormObjectFactory;

/**
 *
 * @author thronecth
 */
public class TimedEventFactory extends QuormObjectFactory<TimedEvent> {

    private final Query<TimedEvent> queryName = handler.getDao().read(TimedEvent.class).where("affected = ?");
    private final Query<TimedEvent> queryBaneID = handler.getDao().read(TimedEvent.class).where("baneeventid = ?");
    private Query<TimedEvent> queryType;
    private final List<BaneEventType> validTypes = Arrays.asList(BaneEventType.TEMPBAN, BaneEventType.MUTE);

    public TimedEventFactory(QuormHandler handler) {
        super(handler, TimedEvent.class);
    }

    @Override
    public Map<Integer, ? extends TimedEvent> getIndex() {
        return this.index;
    }

    public Query<TimedEvent> getQueryName() {
        return queryName;
    }

    private String prepareTypes(List<BaneEventType> types) {
        StringBuilder sb;
        sb = new StringBuilder("(");
        String prefix = "";
        for (BaneEventType type : types) {
            sb.append(prefix);
            prefix = ",";
            sb.append(type.name());
        }
        sb.append(")");
        return sb.toString();
    }

    public List<BaneEventType> getValidTypes() {
        return validTypes;
    }

    public <T extends TimedEvent> T getFromBaneEventId(int id) throws SQLException, Exception {
        PreparedQuery<TimedEvent> prepare = queryBaneID.prepare();
        prepare.set(0, id);
        List<TimedEvent> readAll = prepare.read().readAll();
        if (readAll != null && !readAll.isEmpty()) {
            for (TimedEvent event : readAll) {
                switch (event.getEventType()) {
                    case TEMPBAN:
                        TempBanEvent temp = new TempBanEvent();
                        temp.setId(event.getId());
                        temp.setBaneEventID(event.getBaseEvent().getId());
                        temp.setActive(event.isActive());
                        temp.setUntil(event.getUntil());
                        return (T) temp;
                    case MUTE:
                        MuteEvent mute = new MuteEvent();
                        mute.setId(event.getId());
                        mute.setBaneEventID(event.getBaseEvent().getId());
                        mute.setActive(event.isActive());
                        mute.setUntil(event.getUntil());
                        return (T) mute;

                }
            }
        }
        throw new Exception("Unable to locate timed event with that id: " + id);
    }
}
