package me.ctharvey.Bane.Events;
// Generated Jan 18, 2013 1:07:10 AM by Hibernate Tools 3.2.1.GA

import com.adamantdreamer.foundation.core.db.PlayerID;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.Bane.Bane;
import me.ctharvey.Bane.BaneController;
import me.ctharvey.Helper.Database.Database;
import me.ctharvey.Helper.Database.Table;
import me.ctharvey.Helper.Database.TableAddValues;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 * FlagEvent generated by hbm2java
 */
public final class FlagEvent extends BE implements java.io.Serializable {

    private int id;
    private boolean active;

    public FlagEvent(String reason, String bannedBy, PlayerID playerBanned) {
        super(reason, BaneEventType.FLAG, bannedBy, playerBanned);
        super.insertDB();
        this.id = super.getId();
        active = true;
        this.insertDB();
    }

    public FlagEvent(int id, boolean active, String reason, BaneEventType type, String initiatedBy, PlayerID affected) {
        super(reason, type, initiatedBy, affected);
        this.id = id;
        this.active = active;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
        updateDB();
    }

    @Override
    public void insertDB() {
        Database db = Bane.getSQLDatabase();
        try {
            Table table = db.getTable("flag_event");
            TableAddValues tav = new TableAddValues();
            tav.addValue("id", id);
            tav.addValue("active", active);
            table.addValues(tav);
        } catch (Exception ex) {
            Logger.getLogger(FlagEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void updateDB() {
        try {
            super.updateDB();
            Table table = Bane.getSQLDatabase().getTable("flag_event");
            TableAddValues tav = new TableAddValues();
            tav.addValue("id", id);
            int bool = 0;
            if (active == true) {
                bool = 1;
            }
            tav.addValue("active", bool);
            table.getColumn("id").set(tav, Integer.toString(id));
        } catch (Exception ex) {
            Logger.getLogger(BanEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public static FlagEvent get(int id) {
        BE be = BE.getBE(id);
        try {
            ResultSet values = Bane.getSQLDatabase().getTable("flag_event").getColumn("id").getOneRow(id);
            values.first();
            boolean active = values.getBoolean("active");
            return new FlagEvent(id, active, be.getReason(), be.getEventType(), be.getInitiatedBy(), be.getPlayerID());
        } catch (Exception ex) {
            Logger.getLogger(FlagEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public void onLoginExecute(PlayerLoginEvent e) {
        if (this.isActive()) {
            BaneController.broadcastMessage("bane.flag", "[&6FLAG&f] " + this.getPlayerID().getName() + " flagged for " + this.getReason());
        }
    }

}
