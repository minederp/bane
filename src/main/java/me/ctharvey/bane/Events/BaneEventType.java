/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.events;

import com.adamantdreamer.foundation.core.db.PlayerID;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.nontimed.BanEvent;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.FlagEvent;
import me.ctharvey.bane.events.nontimed.KickEvent;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.bane.events.nontimed.PermBanEvent;
import me.ctharvey.bane.events.nontimed.WarningEvent;
import me.ctharvey.bane.events.timed.MuteEvent;
import me.ctharvey.bane.events.timed.TempBanEvent;
import me.ctharvey.bane.events.timed.TimedEvent;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import org.bukkit.ChatColor;

/**
 *
 * @author Charles
 */
public enum BaneEventType implements Serializable {

    FLAG("Flag", ChatColor.GOLD, FlagEvent.class),
    WARN("Warn", ChatColor.YELLOW, WarningEvent.class),
    MUTE("Mute", ChatColor.BLUE, MuteEvent.class),
    KICK("Kick", ChatColor.DARK_GREEN, KickEvent.class),
    TEMPBAN("TempBan", ChatColor.RED, TempBanEvent.class),
    BAN("Ban", ChatColor.DARK_RED, BanEvent.class),
    PERMBAN("PermBan", ChatColor.STRIKETHROUGH, PermBanEvent.class);

    private final String name;
    private final ChatColor color;
    private Class clazz;

    BaneEventType(String name, ChatColor color, Class clazz) {
        this.name = name;
        this.color = color;
        this.clazz = clazz;
    }

    public String getName() {
        return name;
    }

    public static BaneEventType getByType(String type) {
        for (BaneEventType bType : BaneEventType.values()) {
            if (bType.getName().equalsIgnoreCase(type)) {
                return bType;
            }
        }
        return null;
    }

    public ChatColor getColor() {
        return color;
    }

    public <T extends BaneEventInterface> T getBaneEvent(Class<T> clazz, String reason, PlayerID initiatedBy, PlayerID playerId, JSONableList logs) {
        try {
            return clazz.cast(this.clazz.getConstructor(String.class, PlayerID.class, PlayerID.class, JSONableList.class).newInstance(reason, initiatedBy, playerId, logs));
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(BaneEventType.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public <k extends BaneEventInterface> k convertToChild(Class<k> object, BaneEventInterface event) {
        return (k) event;
    }

    public <T extends BaneEventInterface> BaneEventInterface getBaneEvent(NonTimedEvent event) {
        BaneEventInterface baneEvent = this.getBaneEvent(this.clazz, event.getReason(), event.getInitiatedBy(), event.getAffected(), event.getLogs());
        if (baneEvent instanceof TimedEvent) {
            try {
                baneEvent = Bane.getController().getTimedFactory().getFromBaneEventId(event.getId());
            } catch (Exception ex) {
                Bane.getPlugin().logError("Got instance of Timed Event but unable to load it.");
            }
        } else {
            baneEvent.getBaseEvent().setTime(event.getTime());
            baneEvent.getBaseEvent().setId(event.getId());
            baneEvent.getBaseEvent().setActive(event.isActive());
        }
        return baneEvent;
    }

    public static List<BaneEventType> reverseValues() {
        List<BaneEventType> list = new ArrayList(Arrays.asList(BaneEventType.values()));
        Collections.reverse(list);
        return list;
    }

    public Class getClazz() {
        return clazz;
    }

}
