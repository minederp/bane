/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.events.nontimed;

import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.quorm.query.PreparedQuery;
import com.adamantdreamer.quorm.query.Query;
import com.adamantdreamer.quorm.query.QueryResult;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.timed.TimedEvent;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.mdbase.database.QuormQueryBuilder;
import me.ctharvey.mdbase.enums.EnumHandler;
import me.ctharvey.mdbase.enums.list.ListHandler;
import me.ctharvey.quormdata.factories.QuormObjectFactory;

/**
 *
 * @author thronecth
 */
public class NonTimedEventFactory extends QuormObjectFactory<NonTimedEvent> {

    private final Query<NonTimedEvent> queryName;
    private Query<NonTimedEvent> queryType;
    private final List<BaneEventType> validTypes;

    public NonTimedEventFactory(QuormHandler handler) {
        super(handler, NonTimedEvent.class);
        this.validTypes = new ArrayList<>(Arrays.asList(BaneEventType.BAN, BaneEventType.FLAG, BaneEventType.KICK, BaneEventType.PERMBAN, BaneEventType.WARN));
        this.queryName = handler.getDao().read(NonTimedEvent.class).where("affected = ?");
        this.init();
    }

    @Override
    public Map<Integer, ? extends NonTimedEvent> getIndex() {
        return index;
    }

    public Query<NonTimedEvent> getQueryName() {
        return queryName;
    }

    public List<BaneEventInterface> getQueryType(PlayerID affected, List<BaneEventType> types) {
        List<BaneEventType> mutableList = new ArrayList(types);
        List<NonTimedEvent> readAll = null;
        if (mutableList.isEmpty()) {
            return new ArrayList();
        }
        String questionMarks = QuormQueryBuilder.generatePlaceHolders(mutableList.size());
        this.queryType = handler.getDao().read(NonTimedEvent.class).where("affected = ? AND eventtype IN (" + questionMarks + ")");
        List<String> atypes = ListHandler.toString(EnumHandler.toOrdinalList(mutableList));
        List<Object> queryValues = new ArrayList();
        queryValues.add(affected.getId());
        queryValues.addAll(atypes);
        try {
            PreparedQuery<NonTimedEvent> prepare = queryType.prepare();
            String sql = queryType.getString();
            for (int x = 0; x < queryValues.size(); x++) {
                int indexOf = sql.indexOf("?");
                StringBuilder sb = new StringBuilder();
                String part1 = sql.substring(0, indexOf);
                sb.append(part1).append(queryValues.get(x));
                sb.append(sql.substring(indexOf + 1, sql.length()));
                sql = sb.toString();
                prepare.set(x, queryValues.get(x));
            }
            QueryResult<NonTimedEvent> read = prepare.read();
            readAll = read.readAll();
        } catch (SQLException ex) {
            Logger.getLogger(NonTimedEventFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<BaneEventInterface> events = new ArrayList();
        if (readAll == null || readAll.isEmpty()) {
            return new ArrayList();
        }
        for (NonTimedEvent event : readAll) {
            if (!this.validTypes.contains(event.getEventType())) {
                try {
                    TimedEvent fromBaneEventId = Bane.getController().getTimedFactory().getFromBaneEventId(event.getId());
                    fromBaneEventId.setBaneEventID(event.getId());
                    fromBaneEventId.getBaseEvent().setId(event.getId());
                    events.add(fromBaneEventId);
                } catch (Exception ex) {
                    Logger.getLogger(NonTimedEventFactory.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                BaneEventInterface baneEvent = event.getEventType().getBaneEvent(event);
                boolean add = events.add(baneEvent);
            }
        }
        return events;
    }
    

    public List<BaneEventType> getValidTypes() {
        return validTypes;
    }

}
