/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.events.nontimed;

import com.adamantdreamer.foundation.core.db.PlayerID;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 *
 * @author thronecth
 */
public class PermBanEvent extends NonTimedEvent {

    public PermBanEvent(String reason, PlayerID initiatedBy, PlayerID playerId, JSONableList logs) {
        super(reason, initiatedBy, playerId, BaneEventType.PERMBAN, logs);
    }

    @Override
    public void run() {
        if (Bukkit.getPlayer(getAffected().getUUID()) != null) {
            Bane.getController().asyncKickPlayer(getInitiatedBy(), getAffected(),  "&cYou were permanently banned by &c" + getInitiatedBy().getName() + " &f-- '&c" + getReason() + "&f'. You were banned by an OP.  You are not welcome here.  Do not come back.");
        }
        Bane.getPlugin().broadcast("banish.view", "&2PERMBAN: &6" + getAffected().getName() + " &2by &c" + getInitiatedBy().getName());
    }

    @Override
    public void fail() {
        Map<BaneEventType, List<BaneEventInterface>> baneEvents = Bane.getController().getBaneEvents(this.getAffected(), Arrays.asList(BaneEventType.BAN));
        for (List<BaneEventInterface> events : baneEvents.values()) {
            for (BaneEventInterface event : events) {
                event.getBaseEvent().setActive(false);
            }
        }
        if (getInitiatedBy().getName().equals("CONSOLE")) {
            Bane.getPlugin().log("Unable to locate " + getAffected().getName() + " currently online.  Banning offline.");
        } else {
            Bane.getPlugin().sendMsg("Unable to locate " + getAffected().getName() + " currently online.  Banning offline.", Bukkit.getPlayer(getInitiatedBy().getUUID()));
        }
        run();
    }

    @Override
    public void onLogin(Player player, PlayerLoginEvent event) {
        if (this.isActive()) {
            Map<BaneEventType, List<BaneEventInterface>> baneEvents = Bane.getController().getBaneEvents(this.getAffected(), Arrays.asList(BaneEventType.BAN));
            for (List<BaneEventInterface> events : baneEvents.values()) {
                for (BaneEventInterface aevent : events) {
                    aevent.getBaseEvent().setActive(false);
                }
            }
            executeHardBan(event);
        }
    }

    private void executeHardBan(PlayerLoginEvent e) {
        String message = ChatColor.translateAlternateColorCodes('&', "&cYou were permanently banned by &c" + getInitiatedBy().getName() + " &f-- '&c" + getReason() + "&f'. You were banned by an OP.  You are not welcome here.  Do not come back.");
        if (message.length() > 256) {
            message = message.substring(0, 255);
        }
        e.setKickMessage(ChatColor.RED + message);
        e.disallow(PlayerLoginEvent.Result.KICK_BANNED, ChatColor.RED + message);
        e.getPlayer().kickPlayer(ChatColor.RED + message);
        Bane.getPlugin().log(ChatColor.RED + "Disconnecting " + this.getAffected().getName() + ": PERMBANNED by " + this.getInitiatedBy().getName() + " -- " + this.getReason());
    }

}
