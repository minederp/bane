/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.ctharvey.bane.events.nontimed;

import me.ctharvey.multiserv.packet.JSONable;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 *
 * @author thronecth
 */
public interface BaneEventInterface extends JSONable {

    public NonTimedEvent getBaseEvent();
    
    public void run();
    
    public void fail();
    
    public void onLogin(Player player, PlayerLoginEvent event);

}
