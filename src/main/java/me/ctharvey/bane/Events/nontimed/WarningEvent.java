/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.events.nontimed;

import com.adamantdreamer.foundation.core.db.PlayerID;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerLoginEvent;

/**
 *
 * @author thronecth
 */
public class WarningEvent extends NonTimedEvent {

    public WarningEvent(String reason, PlayerID initiatedBy, PlayerID playerId, JSONableList logs) {
        super(reason, initiatedBy, playerId, BaneEventType.WARN, logs);
    }

    @Override
    public void run() {
        NonTimedEvent event = getBaseEvent();
        if (Bukkit.getPlayer(event.getAffected().getUUID()) != null) {
            Bane.getPlugin().sendMsg(Bukkit.getPlayer(event.getAffected().getUUID()), "&c### You were warned by &b" + event.getInitiatedBy().getName() + " &cfor: &b" + event.getReason() + " &c###");
            Bane.getPlugin().broadcast("banish.view", "&2Warning: &6" + event.getAffected().getName() + "&2 by &c" + event.getInitiatedBy().getName() + "&2 for &b" + event.getReason());
        }
    }

    @Override
    public void fail() {
        NonTimedEvent event = getBaseEvent();
        String reason = "Unable to locate player with name " + event.getAffected().getName();
        if (event.getInitiatedBy().getName().equals("CONSOLE")) {
            Bane.getPlugin().log(reason);
            return;
        }
        Bane.getPlugin().sendMsg(Bukkit.getPlayer(event.getInitiatedBy().getUUID()), reason);
    }

    @Override
    public void onLogin(Player player, PlayerLoginEvent e) {
    }

}
