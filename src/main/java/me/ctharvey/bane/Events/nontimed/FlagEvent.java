package me.ctharvey.bane.events.nontimed;
// Generated Jan 18, 2013 1:07:10 AM by Hibernate Tools 3.2.1.GA

import com.adamantdreamer.foundation.core.db.PlayerID;
import java.lang.ref.WeakReference;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerLoginEvent;

public final class FlagEvent extends NonTimedEvent {

    public FlagEvent(String reason, PlayerID initiatedBy, PlayerID playerId, JSONableList logs) {
        super(reason, initiatedBy, playerId, BaneEventType.FLAG, logs);
    }

    @Override
    public void run() {
        if (Bukkit.getPlayer(this.getAffected().getUUID()) != null) {
            WeakReference<Player> player = new WeakReference<>(Bukkit.getPlayer(this.getAffected().getUUID()));
            Bane.getController().getFlagController().getCurrentFlags().put(player, this);
            Bane.getPlugin().broadcast("banish.flag", "&2Flag: &6" +this.getAffected().getName() + " &2by &c" + this.getAffected().getName() + " &2for &6" + this.getReason());
        }
    }

    @Override
    public void fail() {
        String reason = "Unable to locate player.  Will be flagged anyways";
        if (this.getInitiatedBy().getName().equals("CONSOLE")) {
            Bane.getPlugin().log(reason);
            return;
        }
        Bane.getPlugin().sendMsg(Bukkit.getPlayer(getInitiatedBy().getUUID()), reason);
        run();
    }

    @Override
    public void onLogin(Player player, PlayerLoginEvent e) {
        WeakReference<Player> wPlayer = new WeakReference<>(Bukkit.getPlayer(this.getAffected().getUUID()));
        Bane.getController().getFlagController().getCurrentFlags().put(wPlayer, this);
        Bane.getPlugin().broadcast("banish.flag", "[&cLogin&f] &2Flag: &6" +this.getAffected().getName() + " &2by &c" + this.getAffected().getName() + " &2for &6" + this.getReason());
    }

}
