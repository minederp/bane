/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.events.nontimed;

import com.adamantdreamer.foundation.core.db.PlayerID;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.BaneController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.dataobjects.jsonable.JSONableList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 *
 * @author thronecth
 */
public class KickEvent extends NonTimedEvent {

    public KickEvent(String reason, PlayerID initiatedBy, PlayerID playerId, JSONableList logs) {
        super(reason, initiatedBy, playerId, BaneEventType.KICK, logs);
    }

    @Override
    public void run() {
        if (Bukkit.getPlayer(getAffected().getUUID()) != null) {
            Bane.getController().asyncKickPlayer(getInitiatedBy(), getAffected(), "&6You were kicked by &b" + getInitiatedBy().getName() + " &6for: &b" + getReason());
            Bane.getPlugin().broadcast("banish.view", "&2Kick: &6" + getAffected().getName() + " &2by &c"+getInitiatedBy().getName() + "&2for &6" + getReason());
        }
    }

    @Override
    public void fail() {
        if (getInitiatedBy().getName().equals("CONSOLE")) {
            Bane.getPlugin().log("No player online by that name.");
            return;
        }
        Bane.getPlugin().sendMsg(Bukkit.getPlayer(getInitiatedBy().getUUID()), "No player on by that name.");
    }
}
