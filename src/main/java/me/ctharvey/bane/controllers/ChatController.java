/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.controllers;

import me.ctharvey.bane.BaneController;
import com.adamantdreamer.foundation.core.db.PlayerID;
import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.timed.MuteEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 *
 * @author thronecth
 */
public class ChatController implements Listener {

    private static final Map<Integer, MuteEvent> currentlyMuted = new HashMap();

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent e) {
        try {
            PlayerID id = PlayerIDS.get(e.getPlayer());
            if (currentlyMuted.containsKey(id.getId())) {
                if (currentlyMuted.get(id.getId()).getUntil() > BaneController.getCurrentUnixStamp()) {
                    e.setCancelled(true);
                    Bane.getPlugin().sendMsg("You are currently muted for " + currentlyMuted.get(id.getId()).getTimeLeft(), e.getPlayer());
                    Bane.getPlugin().log(e.getPlayer().getName() + " tried to talk but is currently muted for "+currentlyMuted.get(id.getId()).getTimeLeft());
                } else {
                    currentlyMuted.get(id.getId()).setActive(false);
                    currentlyMuted.get(id.getId()).update();
                    currentlyMuted.remove(id.getId());
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BaneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Map<Integer, MuteEvent> getCurrentlyMuted() {
        return currentlyMuted;
    }
}
