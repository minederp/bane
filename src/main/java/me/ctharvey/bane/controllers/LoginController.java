/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.controllers;

import com.adamantdreamer.foundation.core.db.PlayerIDS;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author thronecth
 */
public class LoginController implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onJoin(PlayerLoginEvent e) {
        try {
            Map<BaneEventType, List<BaneEventInterface>> baneEvents = Bane.getController().getBaneEvents(PlayerIDS.get(e.getPlayer()));
            for (BaneEventType type : BaneEventType.reverseValues()) {
                if (baneEvents.containsKey(type)) {
                    List<BaneEventInterface> get = baneEvents.get(type);
                    Collections.reverse(get);
                    for (BaneEventInterface event : get) {
                        if (event.getBaseEvent().isActive() && e.getResult().equals(Result.ALLOWED)) {
                            event.onLogin(e.getPlayer(), e);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onLeave(PlayerQuitEvent e) {
        e.setQuitMessage(null);;
    }

}
