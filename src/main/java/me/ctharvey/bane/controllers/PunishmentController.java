/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane.controllers;

import com.adamantdreamer.foundation.core.db.PlayerID;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

/**
 *
 * @author thronecth
 */
public class PunishmentController implements Listener {

    private static final Map<PlayerID, BaneEventInterface> recentEvent = new HashMap<>();
    private final static int timeValid = 60;

    public static Map<PlayerID, BaneEventInterface> getRecentEvent() {
        return recentEvent;
    }
    
    public static void addPunishMent(BaneEventInterface event){
        NonTimedEvent baseEvent = event.getBaseEvent();
        recentEvent.put(baseEvent.getAffected(), event);
    }

    public static boolean punishedRecently(PlayerID uuid, BaneEventType type, CommandSender cs) {
        if (recentEvent.containsKey(uuid)) {
            NonTimedEvent nte = recentEvent.get(uuid).getBaseEvent();
            if (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - nte.getTime() < 60) {
                if (type.ordinal() <= nte.getBaseEvent().getEventType().ordinal()) {
                    sendPunishedRecentlyMSG(cs);
                    return true;
                }
            }
        }
        return false;
    }

    private static void sendPunishedRecentlyMSG(CommandSender cs) {
        Bane.getPlugin().sendMsg("This person has already been issued an equal or higher infraction.", cs);
    }

}
