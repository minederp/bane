/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.ctharvey.bane;

import com.adamantdreamer.foundation.core.db.PlayerID;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.ctharvey.bane.Bane;
import me.ctharvey.bane.appeals.AppealsHashFactory;
import me.ctharvey.bane.controllers.ChatController;
import me.ctharvey.bane.controllers.FlagController;
import me.ctharvey.bane.controllers.LoginController;
import me.ctharvey.bane.controllers.PunishmentController;
import me.ctharvey.bane.events.BaneEventType;
import me.ctharvey.bane.events.nontimed.BaneEventInterface;
import me.ctharvey.bane.events.nontimed.NonTimedEvent;
import me.ctharvey.bane.events.nontimed.NonTimedEventFactory;
import me.ctharvey.bane.events.timed.TimedEvent;
import me.ctharvey.bane.events.timed.TimedEventFactory;
import me.ctharvey.bane.multiserve.BanePacket;
import me.ctharvey.mdbase.database.QuormHandler;
import me.ctharvey.mdbase.multiserver.PlayerIDHandler;
import me.ctharvey.multiserv.client.MultiServeClient;
import me.ctharvey.onlineusers.OnlineUsers;
import me.ctharvey.onlineusers.userinfo.OnlineUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Charles
 */
public class BaneController implements Listener {

    private final QuormHandler handler;

    private NonTimedEventFactory nonTimedFactory;
    private TimedEventFactory timedFactory;
    private AppealsHashFactory appealsHashFactory;

    private LoginController loginController = new LoginController();
    private FlagController flagController = new FlagController();
    private ChatController chatController = new ChatController();
    private PunishmentController punishController = new PunishmentController();

    public BaneController(QuormHandler handler) {
        this.handler = handler;
        this.nonTimedFactory = new NonTimedEventFactory(handler);
        this.timedFactory = new TimedEventFactory(handler);
        this.appealsHashFactory = new AppealsHashFactory(handler);
        Plugin plugin = Bane.getPlugin();
        Bukkit.getPluginManager().registerEvents(loginController, plugin);
        Bukkit.getPluginManager().registerEvents(flagController, plugin);
        Bukkit.getPluginManager().registerEvents(chatController, plugin);
        Bukkit.getPluginManager().registerEvents(punishController, plugin);
    }

    public NonTimedEventFactory getNonTimedFactory() {
        return nonTimedFactory;
    }

    public TimedEventFactory getTimedFactory() {
        return timedFactory;
    }

    public Map<BaneEventType, List<BaneEventInterface>> getBaneEvents(PlayerID affected) {
        return getBaneEvents(affected, Arrays.asList(BaneEventType.values()));
    }

    public <T extends BaneEventInterface> List<T> getEventType(PlayerID affected, BaneEventType type, Class<T> clazz) {
        List<BaneEventInterface> baneEvents = getBaneEvents(affected, Arrays.asList(type)).get(type);
        return (List<T>) baneEvents;
    }

    public Map<BaneEventType, List<BaneEventInterface>> getBaneEvents(PlayerID affected, List<BaneEventType> types) {
        if (types.isEmpty()) {
            return new HashMap();
        }
        List<BaneEventInterface> allEvents = getNonTimedFactory().getQueryType(affected, types);
        Map<BaneEventType, List<BaneEventInterface>> events = new HashMap();
        for (BaneEventInterface bei : allEvents) {
            NonTimedEvent event = bei.getBaseEvent();
            if (!events.containsKey(event.getEventType())) {
                events.put(event.getEventType(), new ArrayList());
            }
            events.get(event.getEventType()).add(bei);
        }
        return events;
    }

    public static void processEvent(BaneEventInterface event) {
        processEvent(event, true);
    }

    public static void processEvent(BaneEventInterface event, boolean newEvent) {
        if (!PunishmentController.punishedRecently(event.getBaseEvent().getAffected(), event.getBaseEvent().getEventType(), Bukkit.getPlayer(event.getBaseEvent().getInitiatedBy().getUUID()))) {
            OnlineUser user = OnlineUsers.getPlayersServer(event.getBaseEvent().getAffected());
            if (newEvent) {
                if (event instanceof TimedEvent) {
                    ((TimedEvent) event).create();
                } else {
                    ((NonTimedEvent) event).create();
                }
            }
            if (user != null && user.getServer().equals(Bukkit.getServerName())) {
                event.run();
            } else {
                if (user != null) {
                    if (user.getPlayerName() != null && user.getServer() != null) {
                        MultiServeClient.sendPacket(new BanePacket(user.getServer(), Bukkit.getServerName(), new BanePacket.BEContainer(event.getBaseEvent().getEventType(), event.getBaseEvent())));
                    }
                } else {
                    event.fail();
                }
            }
        }
    }

    public static long getCurrentUnixStamp() {
        return System.currentTimeMillis() / 1000L;
    }

    public LoginController getLoginController() {
        return loginController;
    }

    public FlagController getFlagController() {
        return flagController;
    }

    public ChatController getChatController() {
        return chatController;
    }

    public PunishmentController getPunishController() {
        return punishController;
    }

    public AppealsHashFactory getAppealsHashFactory() {
        return appealsHashFactory;
    }

    public void asyncKickPlayer(PlayerID initated, final PlayerID affected, final String msg) {
        Bukkit.getScheduler().runTask(Bane.getPlugin(), new Runnable() {

            @Override
            public void run() {
                Bukkit.getPlayer(affected.getUUID()).kickPlayer(ChatColor.translateAlternateColorCodes('&', msg));
            }
        });

    }

}
